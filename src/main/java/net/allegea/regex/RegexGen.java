package net.allegea.regex;

import net.allegea.regex.interpreter.Parser;

public class RegexGen {
  
	public static String of(String pattern) {
		return Parser.parse(pattern).reduce();
	}
}
