package net.allegea.regex.interpreter;

public enum Exp {
    LITERAL,
    UNION,
    INTERSECTION,
    RANDOM,
    QUANTIFY,
}
